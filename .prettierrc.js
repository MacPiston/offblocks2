module.exports = {
  ...require("@gorrion/prettier-config"),
  semi: false,
};
